Efter att ha haft problem med att våra redirects cachas på olika nivåer (i webbläsare, proxyservrar och hos webbhotell)
gör vi numera på följande sätt:

Vi använder redirects med hjälp av HTTP 302 (moved temporarily), istället för HTTP 301 (moved permanently)
eller Meta Refresh. Det löser följande problem:

* Sökmotorer ska visa /documents/ARK_xxxx/ och inte det fulla filnamnet
* Sökmotorer ska fortsätta att bevaka /documents/ARK_xxxx/ och inte bara indexera den fil som redirecten pekar på
* Caches och proxyservrar ska vara beredda på att målet för våra redirects ändras ibland

Redirects med hjälp av HTTP 301 och Meta Refresh cachas bland annat av många webbläsare för evigt. Det försvaras
bl.a. av Google Chrome med att "HTTP 301 means 'moved permanently' and is not supposed to change".

Rent praktiskt görs redirects med hjälp av en .htaccess-fil i respektive ARK-mapp.