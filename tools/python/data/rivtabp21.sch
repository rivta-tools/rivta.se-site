<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron">
    <title>RIV TA Basic Profile 2.1</title>
    <ns prefix="wsdl" uri="http://schemas.xmlsoap.org/wsdl/" />
    <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema" />
    <ns prefix="soap" uri="http://schemas.xmlsoap.org/wsdl/soap/" />

    <!-- These will be converted to xslt:params, pass the values at transform time. -->
    <let name="domainName" value="'a:b:c'" />
    <let name="majorVersion" value="0" />
    <let name="initiatorOperationName" value="'opInitiator'" />
    <let name="responderOperationName" value="'opResponder'" />
    <let name="interactionName" value="'int'" />

    <let name="expectedInitiatorRequestTypeName" value="concat(':', $initiatorOperationName)"/>
    <let name="expectedInitiatorResponseTypeName" value="concat(':', $initiatorOperationName, 'Response')"/>
    <let name="expectedResponderRequestTypeName" value="concat(':', $responderOperationName)"/>
    <let name="expectedResponderResponseTypeName" value="concat(':', $responderOperationName, 'Response')"/>

    <pattern>
        <title>Regel #3</title>
        <let name="expectedDefinitionsName" value="concat($interactionName, 'Interaction')"/>
        <rule context="wsdl:definitions">
            <report test="@name != $expectedDefinitionsName">
                wsdl:definitions/@name bör vara <value-of select="$expectedDefinitionsName"/>. Was <value-of select="@name"/>.
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #4</title>
        <let name="expectedTargetNamespace1" value="concat('urn:riv:', $domainName, ':', $interactionName, ':', $majorVersion,':rivtabp21' )" />
        <let name="expectedTargetNamespace2" value="concat('urn:riv-application:', $domainName, ':', $interactionName, ':', $majorVersion,':rivtabp21' )" />
		<let name="expectedTargetNamespace3" value="concat('urn:riv-lv:', $domainName, ':', $interactionName, ':', $majorVersion,':rivtabp21' )" />
        <rule context="wsdl:definitions">
            <assert test="@targetNamespace = $expectedTargetNamespace1 or @targetNamespace = $expectedTargetNamespace2 or @targetNamespace = $expectedTargetNamespace3">
                Felaktigt target namespace: <value-of select="@targetNamespace"/>.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #5</title>
        <rule context="wsdl:definitions">
            <report test="count(wsdl:documentation) = 0">
                Interaktioner bör dokumenteras med hjälp av wsdl:documentation
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #7</title>

        <rule context="wsdl:binding/soap:binding">
            <assert test="@style = 'document'">
                "SOAP binding style" skall sättas till "document". Was <value-of select="@style"/>
            </assert>
        </rule>
        <rule context="soap:body">
            <assert test="@use = 'literal'">
                "SOAP body use" skall sättas till "literal". Was <value-of select="@use"/>
            </assert>
        </rule>
        <rule context="wsdl:message">
            <assert test="wsdl:part[@name = 'parameters']">
                Varje wsdl:message skall ha en wsdl:part, som skall vara namnsatt till "parameters".
            </assert>
        </rule>
    </pattern>
        <!--
        <rule context="wsdl:portType/wsdl:operation">
            <let name="inputMessageName" value="wsdl:input/@message"/>
            <let name="outputMessageName" value="wsdl:output/@message"/>
            <assert test="//wsdl:message/[contains($inputMessageName, @name)]/wsdl:part[@name = 'parameters']/@element">

            </assert>
            <assert test="contains(wsdl:part[@name = 'parameters']/@element, $expectedRequestTypeName) or contains(wsdl:part[@name = 'parameters']/@element, $expectedResponseTypeName)">
                Regel #7: Varje wsdl:message skall ha en wsdl:part, som skall vara namnsatt till "parameters".
            </assert>
        </rule>-->

    <pattern>
        <title>Regel #8</title>
        <let name="import" value="//xs:import[@namespace = 'urn:riv:itintegration:registry:1']"/>
        <rule context="wsdl:definitions">
            <assert test="count($import) = 1">
                WSDL skall importera namnrymden "urn:riv:itintegration:registry:1".
            </assert>
            <!--
            <report test="@*[local-name() = 'riv'] != 'urn:riv:itintegration:registry:1'">
                "urn:riv:itintegration:registry:1" bör ges namnrymdsalias "riv"
                was: <value-of select="@*[local-name() = 'riv']" />
            </report>-->
        </rule>
    </pattern>
    <pattern>
        <title>Regel #9</title>
        <let name="expectedInitiatorPortTypeName" value="concat($interactionName, 'InitiatorInterface')" />
        <let name="expectedResponderPortTypeName" value="concat($interactionName, 'ResponderInterface')" />
        <rule context="wsdl:portType">
            <report test="@name != $expectedInitiatorPortTypeName and @name != $expectedResponderPortTypeName">
                Namn på portType-element följer inte rekommendation (<value-of select="@name"/>).
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #10</title>
        <let name="expectedInitiatorBindingName" value="concat($interactionName, 'InitiatorBinding')" />
        <let name="expectedResponderBindingName" value="concat($interactionName, 'ResponderBinding')" />
        <rule context="wsdl:binding">
            <report test="@name != $expectedInitiatorBindingName and @name != $expectedResponderBindingName">
                Namn på service-element följer inte rekommendation (<value-of select="@name"/>).
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #11</title>
        <let name="expectedInitiatorServiceName" value="concat($interactionName, 'InitiatorService')" />
        <let name="expectedResponderServiceName" value="concat($interactionName, 'ResponderService')" />
        <rule context="wsdl:service">
            <report test="@name != $expectedInitiatorServiceName and @name != $expectedResponderServiceName">
                Namn på service-element följer inte rekommendation (<value-of select="@name"/>).
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #12</title>
        <let name="expectedInitiatorPortName" value="concat($interactionName, 'InitiatorPort')" />
        <let name="expectedResponderPortName" value="concat($interactionName, 'ResponderPort')" />
        <rule context="wsdl:port">
            <report test="@name != $expectedInitiatorPortName and @name != $expectedResponderPortName">
                Namn på port-element följer inte rekommendation (<value-of select="@name"/>).
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #13</title>
        <let name="initiatorRequest" value="concat($initiatorOperationName, 'Request' )"/>
        <let name="initiatorResponse" value="concat($initiatorOperationName, 'Response' )"/>
        <let name="responderRequest" value="concat($responderOperationName, 'Request' )"/>
        <let name="responderResponse" value="concat($responderOperationName, 'Response' )"/>
        <rule context="wsdl:message">
            <assert test="@name = $initiatorRequest or @name = $initiatorResponse or @name = $responderRequest or @name = $responderResponse">
                name-attributet på wsdl:message har felaktigt värde (<value-of select="@name"/> )
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #14</title>
        <rule context="wsdl:operation">
            <assert test="@name = $initiatorOperationName or @name = $responderOperationName">
                Felaktigt operation/@name (<value-of select="@name"/>).
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #15</title>
        <rule context="soap:operation">
            <let name="validInitiatorSoapAction1" value="concat('urn:riv:', $domainName, ':', $interactionName, 'Initiator:', $majorVersion, ':', ../@name)" />
            <let name="validInitiatorSoapAction2" value="concat('urn:riv-application:', $domainName, ':', $interactionName, 'Initiator:', $majorVersion, ':', ../@name)" />
			<let name="validInitiatorSoapAction3" value="concat('urn:riv-lv:', $domainName, ':', $interactionName, 'Initiator:', $majorVersion, ':', ../@name)" />
            <let name="validResponderSoapAction1" value="concat('urn:riv:', $domainName, ':', $interactionName, 'Responder:', $majorVersion, ':', ../@name)" />
            <let name="validResponderSoapAction2" value="concat('urn:riv-application:', $domainName, ':', $interactionName, 'Responder:', $majorVersion, ':', ../@name)" />
			<let name="validResponderSoapAction3" value="concat('urn:riv-lv:', $domainName, ':', $interactionName, 'Responder:', $majorVersion, ':', ../@name)" />
            <assert test="@soapAction = $validInitiatorSoapAction1 or @soapAction = $validInitiatorSoapAction2 or @soapAction = $validInitiatorSoapAction3 or @soapAction = $validResponderSoapAction1 or @soapAction = $validResponderSoapAction2 or @soapAction = $validResponderSoapAction3">
                Felaktig soapAction: <value-of select="@soapAction"/>.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #16</title>
        <rule context="xs:schema">
            <let name="definitionsNamespace" value="/wsdl:definitions/@targetNamespace"/>
            <assert test="@targetNamespace = $definitionsNamespace">
                xs:schema och wsdl:definitions skall ha samma targetNamespace.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #17</title>
        <rule context="wsdl:portType">
            <assert test="count(wsdl:operation) &lt;= 1">
                Endast en operation per porttype
            </assert>
        </rule>
        <rule context="wsdl:definitions">
            <assert test="count(wsdl:portType) &lt;= 2">
                Max två porttypes per interaktion
            </assert>
        </rule>
    </pattern>
</schema>