<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <title>RIV Tekniska Anvisningar Domanschema 2.1</title>
    <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema" />

    <!-- These will be converted to xslt:params, pass the values at transform time. -->
    <let name="domainName" value="'a:b:c'" />
    <let name="majorVersion" value="0" />
    <let name="minorVersion" value="0" />
    
    <pattern>
        <title>Regel #3</title>
        <rule context="xs:schema">
            <let name="expectedNamespace1" value="concat('urn:riv:', $domainName, ':', $majorVersion )" />
            <let name="expectedNamespace2" value="concat('urn:riv-application:', $domainName, ':', $majorVersion )" />
            <assert test="@targetNamespace = $expectedNamespace1 or @targetNamespace = $expectedNamespace2">
                Felaktigt targetNamespace: <value-of select="@targetNamespace" />
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #4</title>
        <rule context="xs:schema">
            <let name="expectedVersion" value="concat($majorVersion, '.', $minorVersion)" />
            <report test="@version != $expectedVersion">
                Schema version har inte rekommenderat värde: <value-of select="@version" />.
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #5</title>
        <rule context="xs:complexType/xs:sequence">
            <let name="lastElement" value="*[last()]"/>

            <!-- Ej utökad. Bör avslutas med ANY -->
            <report test="local-name($lastElement) != 'any' and string-length($lastElement/@ref) = 0">
                ComplexType <value-of select="../@name"/> behöver avslutas med ANY för att i framtiden kunna utökas.
            </report>

            <assert test="local-name($lastElement) != 'any' or ($lastElement/@namespace = '##other' and $lastElement/@processContents = 'lax' and $lastElement/@minOccurs = '0' and $lastElement/@maxOccurs = 'unbounded')">
                Any-elementet i <value-of select="../@name"/> har felaktiga attribut.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #6</title>
        <rule context="xs:complexType/xs:sequence">
            <let name="previousElement" value="*[last()-1]"/>
            <let name="lastElement" value="*[last()]"/>

            <!-- Icke-bakåtkompatibel utökning. Bör avslutas med ANY.-->
            <report test="local-name($lastElement) != 'any' and string-length($lastElement/@ref) > 0 and number($lastElement/@minOccurs) > 0">
                ComplexType <value-of select="../@name"/> är utökad, men kan fortfarande avslutas med ANY för att i framtiden kunna utökas ännu en gång.
            </report>

            <!-- Bakåtkompatibel utökning. Får inte avslutas med ANY -->
            <assert test="not(local-name($lastElement) = 'any' and string-length($previousElement/@ref) > 0 and $previousElement/@minOccurs = '0')">
                ComplexType <value-of select="../@name"/> har utökats på ett ej tillåtet sätt. Any-elementet måste tas bort.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #7</title>
        <let name="validChars" value="'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.:-_'"/>
        <rule context="xs:element|xs:attribute|xs:simpleType|xs:complexType">
            <assert test="translate(@name, $validChars, '') = ''">
                Element- och attributnamn ska inte innehålla nationella tecken.
                (<value-of select="@name" />)
            </assert>
        </rule>
        <rule context="xs:enumeration">
            <assert test="translate(@value, $validChars, '') = ''">
                Enumerations ska inte innehålla nationella tecken.
                (<value-of select="@value" />)
            </assert>
        </rule>
    </pattern>
</schema>