<?xml version="1.0" encoding="utf-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <title>RIV Tekniska Anvisningar Tjansteschema 2.1</title>
    <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema" />

    <!-- These will be converted to xslt:params, pass the values at transform time. -->
    <let name="domainName" value="'a:b:c'" />
    <let name="majorVersion" value="0" />
    <let name="minorVersion" value="0" />
    <let name="operationName" value="'op'" />
    <let name="interactionName" value="'op'" />
    <let name="interactionRole" value="'role'" />

    <let name="expectedRequestElementName" value="$operationName" />
    <let name="expectedRequestTypeName" value="concat($operationName, 'Type')" />
    <let name="expectedResponseElementName" value="concat($operationName, 'Response')" />
    <let name="expectedResponseTypeName" value="concat($operationName, 'ResponseType')" />

    <pattern>
        <title>Regel #3</title>
        <rule context="xs:schema">
            <!-- Correct namespace example: urn:riv:crm:scheduling:MakeBookingResponder:1 -->
            <let name="expectedNamespace1" value="concat('urn:riv:', $domainName, ':', $interactionName, $interactionRole, ':', $majorVersion )" />
            <let name="expectedNamespace2" value="concat('urn:riv-application:', $domainName, ':', $interactionName, $interactionRole, ':', $majorVersion )" />
            <assert test="@targetNamespace = $expectedNamespace1  or @targetNamespace = $expectedNamespace2">
                Felaktigt targetNamespace: <value-of select="@targetNamespace" />
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #4</title>
        <rule context="xs:schema/xs:element">
            <assert test="@name = $expectedRequestElementName or @name = $expectedResponseElementName">
                Felaktigt namn på element: <value-of select="@name"/>.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #5: namn på typer</title>
        <rule context="xs:schema">
            <let name="requestElement" value="xs:element[@name = $expectedRequestElementName]" />
            <let name="responseElement" value="xs:element[@name = $expectedResponseElementName]" />

            <report test="count($requestElement) > 0 and not(contains($requestElement/@type, $expectedRequestTypeName))">
                Request-typer bör ha namnet '<value-of select="$expectedRequestTypeName" />'
                (was: <value-of select="$requestElement/@type"/>).
            </report>

            <assert test="count($responseElement) = 0 or contains($responseElement/@type, $expectedResponseTypeName)">
                Response-typen skall ha namnet '<value-of select="$expectedResponseTypeName" />'
                (was: <value-of select="$responseElement/@type"/>).
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #6</title>
        <rule context="xs:schema">
            <assert test="@elementFormDefault = 'qualified'">
                elementFormDefault skall vara 'qualified', was: '<value-of select="@elementFormDefault"/>'.
            </assert>

            <assert test="@attributeFormDefault = 'unqualified'">
                attributeFormDefault skall vara 'unqualified', was: '<value-of select="@attributeFormDefault"/>'.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #7</title>
        <rule context="xs:schema">
            <let name="expectedVersion" value="concat($majorVersion, '.', $minorVersion)" />
            <report test="@version != $expectedVersion">
                schema version bör sättas till <value-of select="$expectedVersion" />
                (was: <value-of select="@version" />)
            </report>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #8</title>
        <rule context="xs:complexType/xs:sequence">
            <let name="lastElement" value="*[last()]"/>

            <!-- Ej utökad. Bör avslutas med ANY -->
            <report test="local-name($lastElement) != 'any' and string-length($lastElement/@ref) = 0">
                ComplexType <value-of select="../@name"/> behöver avslutas med ANY för att i framtiden kunna utökas.
            </report>

            <assert test="local-name($lastElement) != 'any' or ($lastElement/@namespace = '##other' and $lastElement/@processContents = 'lax' and $lastElement/@minOccurs = '0' and $lastElement/@maxOccurs = 'unbounded')">
                Any-elementet i <value-of select="../@name"/> har felaktiga attribut.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #9</title>
        <rule context="xs:complexType/xs:sequence">
            <let name="previousElement" value="*[last()-1]"/>
            <let name="lastElement" value="*[last()]"/>

            <!-- Icke-bakåtkompatibel utökning. Bör avslutas med ANY.-->
            <report test="local-name($lastElement) != 'any' and string-length($lastElement/@ref) > 0 and number($lastElement/@minOccurs) > 0">
                ComplexType <value-of select="../@name"/> är utökad, men kan fortfarande avslutas med ANY för att i framtiden kunna utökas ännu en gång.
            </report>

            <!-- Bakåtkompatibel utökning. Får inte avslutas med ANY -->
            <assert test="not(local-name($lastElement) = 'any' and string-length($previousElement/@ref) > 0 and $previousElement/@minOccurs = '0')">
                ComplexType <value-of select="../@name"/> har utökats på ett ej tillåtet sätt. Any-elementet måste tas bort.
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #10</title>
        <let name="validChars" value="'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.:-_'"/>
        <rule context="xs:element|xs:attribute|xs:simpleType|xs:complexType">
            <assert test="translate(@name, $validChars, '') = ''">
                Element- och attributnamn ska inte innehålla nationella tecken.
                (<value-of select="@name" />)
            </assert>
        </rule>
        <rule context="xs:enumeration">
            <assert test="translate(@value, $validChars, '') = ''">
                Enumerations ska inte innehålla nationella tecken.
                (<value-of select="@value" />)
            </assert>
        </rule>
    </pattern>
    <pattern>
        <title>Regel #11</title>
        <rule context="xs:complexType">
            <report test="@name = $expectedResponseTypeName and starts-with(@name, 'Get') and xs:sequence/*[contains(@name, 'result')]">
                Enbart uppdaterande tjänster ska innehålla result-koder i svarsmeddelandet. (<value-of select="@name" />)
            </report>
        </rule>
    </pattern>
</schema>